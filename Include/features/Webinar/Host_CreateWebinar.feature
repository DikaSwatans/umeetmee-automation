Feature: Host Create Webinar
  As Host, i want to Create Webinar

  Background: Open UMeetMe Web
    * I navigate to HomePage

  Scenario: Verify Dashboard
    ## Verify Input Phone Number & Dialog Check ##
    * I am navigate to dashboard
    * I should see 'DashboardWebinar/Button_Webinar'
    * I click 'DashboardWebinar/Button_Webinar'
    * I should see 'DashboardWebinar/Button_Create_Webinar'
    * I click 'DashboardWebinar/Button_Create_Webinar'
    * I should see 'DashboardWebinar/Text_CreateNewWebinar'
    * I type 'FAB' on 'DashboardWebinar/input_Info_title'
    * I type 'FAB Webinar' on 'DashboardWebinar/Field_Description'
    * I click 'DashboardWebinar/input_Invite'
    * I type 'dswatans@gmail.com' on 'DashboardWebinar/input_Invite'
    * I enter 'DashboardWebinar/input_Invite'
    * I click 'DashboardWebinar/DatePicker'
    * I click 'DashboardWebinar/Pick_DatePicker'
    * I click 'DashboardWebinar/JamPicker'
    * I click 'DashboardWebinar/Down_JamPicker'
    * I click 'DashboardWebinar/WaitingRoom'
    * I click 'DashboardWebinar/Button_Dash_Create'
