import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.internal.PathUtil as PathUtil

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import io.appium.java_client.AppiumDriver
import io.appium.java_client.android.AndroidDriver
import io.appium.java_client.android.nativekey.AndroidKey
import io.appium.java_client.android.nativekey.KeyEvent
import org.openqa.selenium.Keys as Keys


class Common {
	/**
	 * The step definitions below match with Katalon sample Gherkin steps
	 */

	@Given("I navigate to HomePage")
	def I_navigate_to_HomePage() {
		WebUI.openBrowser("https://sandbox.umeetme.id/")
		WebUI.maximizeWindow()
		WebUI.delay(5)
	}

	@Given("I navigate to Webinar Room")
	def I_navigate_to_Webinar_Room() {
		WebUI.openBrowser(GlobalVariable.LinkWebinar)
		WebUI.maximizeWindow()
		WebUI.delay(5)
	}
	@When("I should see '(.*)'")
	def I_should_see(String elme) {
		WebUI.verifyElementVisible(findTestObject('Object Repository/'+elme))
		WebUI.delay(5)
	}
	@When("I should not see '(.*)'")
	def I_should_not_see(String elme) {
		WebUI.verifyElementNotVisible(findTestObject('Object Repository/'+elme))
		WebUI.delay(5)
	}
	@When("I click '(.*)'")
	def I_click(String elme) {
		WebUI.click(findTestObject('Object Repository/'+elme))
		WebUI.delay(5)
	}
	@When("I enter '(.*)'")
	def I_enter(String elme) {
		WebUI.sendKeys(findTestObject('Object Repository/'+elme), Keys.chord(Keys.ENTER))
	}

	@When("I type '(.*)' on '(.*)'")
	def I_type_on(String txt, String elme) {
		WebUI.setText(findTestObject('Object Repository/'+elme), txt)
		WebUI.delay(5)
	}

	@When("I press back")
	def I_press_back() {
		WebUI.back()
		WebUI.delay(5)
	}

	@When("I close browser")
	def I_close_browser() {
		WebUI.closeBrowser()
		WebUI.delay(5)
	}

	@When("I am navigate to dashboard host")
	def I_navigate_to_dashboard_host() {
		this.I_should_see('HomePage/Button_DaftarGratis')
		this.I_click('HomePage/Button_Masuk')
		this.I_should_see('HomePage/Text_Masuk')
		this.I_click('HomePage/Field_Telepon')
		this.I_type_on(GlobalVariable.Host, 'HomePage/Field_Telepon')
		this.I_click('HomePage/Field_Password')
		this.I_type_on('qaumeetme', 'HomePage/Field_Password')
		this.I_click('HomePage/Button_Masuk_Login')
		this.I_should_see('HomePage/Button_MyMeeting')
		this.I_click('HomePage/Button_MyMeeting')
		WebUI.delay(6)
	}

	@When("I am navigate to dashboard participant")
	def I_navigate_to_dashboard_participant() {
		this.I_should_see('HomePage/Button_DaftarGratis')
		this.I_click('HomePage/Button_Masuk')
		this.I_should_see('HomePage/Text_Masuk')
		this.I_click('HomePage/Field_Telepon')
		this.I_type_on(GlobalVariable.Participant, 'HomePage/Field_Telepon')
		this.I_click('HomePage/Field_Password')
		this.I_type_on('qaumeetme', 'HomePage/Field_Password')
		this.I_click('HomePage/Button_Masuk_Login')
		this.I_should_see('HomePage/Button_MyMeeting')
		this.I_click('HomePage/Button_MyMeeting')
		WebUI.delay(6)
	}

	@When("I create webinar room")
	def I_create_webinar_room() {
		this.I_should_see('DashboardWebinar/Button_Webinar')
		this.I_click('DashboardWebinar/Button_Webinar')
		this.I_should_see('DashboardWebinar/Button_Create_Webinar')
		this.I_click('DashboardWebinar/Button_Create_Webinar')
		this.I_should_see('DashboardWebinar/Text_CreateNewWebinar')
		this.I_type_on('FAB', 'DashboardWebinar/input_Info_title')
		this.I_type_on('FAB_Webinar', 'DashboardWebinar/Field_Description')
		this.I_click('DashboardWebinar/input_Invite')
		this.I_type_on('dswatans@gmail.com', 'DashboardWebinar/input_Invite')
		this.I_enter('DashboardWebinar/input_Invite')
		this.I_click('DashboardWebinar/DatePicker')
		this.I_click('DashboardWebinar/Pick_DatePicker')
		this.I_click('DashboardWebinar/JamPicker')
		this.I_click('DashboardWebinar/Down_JamPicker')
		this.I_click('DashboardWebinar/WaitingRoom')
		this.I_click('DashboardWebinar/Button_Dash_Create')
		WebUI.delay(6)
	}
}