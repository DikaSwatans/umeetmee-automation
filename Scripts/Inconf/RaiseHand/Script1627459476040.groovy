import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('https://umeetme.id/')

WebUI.maximizeWindow()

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/button_Masuk'))

WebUI.delay(10)

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/a_Masuk'))

WebUI.waitForElementPresent(findTestObject('Page_UMeetMe/input_Atur Kata Sandi_email'), 10)

WebUI.delay(10)

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/input_Atur Kata Sandi_email'), '87732733736')

WebUI.delay(10)

WebUI.setEncryptedText(findTestObject('Object Repository/Page_UMeetMe/input_Nomor Telepon_password'), 'MJgMFow5U6m+bMVXD8qxOA==')

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/button_MASUK (1)'))

WebUI.waitForElementClickable(findTestObject('Page_UMeetMe/a_MY MEETING'), 3)

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/a_MY MEETING'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/div_Selamat DatangHendriawan Swatandika_car_921034'))

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/input_Close_title'), 'FAB')

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/button_Create'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/button_LANJUTKAN'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe - 1062971805/a_Raise Hand'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe - 1062971805/div_More_toolbox-icon'))

