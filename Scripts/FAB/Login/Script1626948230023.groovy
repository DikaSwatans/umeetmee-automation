import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.click(findTestObject('Page_UMeetMe/a_Masuk'))

WebUI.click(findTestObject('Page_UMeetMe/a_Masuk'))

WebUI.setText(findTestObject('Page_UMeetMe/input_Atur Kata Sandi_email'), '82100000010')

WebUI.setEncryptedText(findTestObject('Page_UMeetMe/input_Nomor Telepon_password'), '4gM+uDOggKKAi2ntl3wnJQ==')

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/img_Kata Sandi_toggle-password'))

WebUI.click(findTestObject('Page_UMeetMe/button_Masuk'))

WebUI.click(findTestObject('Page_UMeetMe/a_MY MEETING'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/span_Webinars'))

WebUI.click(findTestObject('Object Repository/Page_UMeetMe/button_Create New Webinars'))

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/input_Webinar Info_title'), 'Dikaa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_Webinar Title_description'), 'd')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_d'), 'ds')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_ds'), 'dsa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsa'), 'dsad')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsad'), 'dsada')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsada'), 'dsadas')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadas'), 'dsadasd')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasd'), 'dsadasda')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasda'), 'dsadasdas')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdas'), 'dsadasdasa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdasa'), 'dsadasdasas')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdasas'), 'dsadasdasasa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdasasa'), 'dsadasdasasas')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdasasas'), 'dsadasdasasasa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/textarea_dsadasdasasasa'), 'dsadasdasasasaa')

WebUI.setText(findTestObject('Object Repository/Page_UMeetMe/input_Invite Speaker_username'), 'adsadsa')

